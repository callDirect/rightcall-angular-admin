'use strict';
var utils = require('./utils');
var exports = require('./exports');
var SocketIoConnection  = require('./socketioconnection');
var debugCallDirect = exports.debugCallDirect;
var RTCIceCandidate= exports.RTCIceCandidate;
var RTCSessionDescription = exports.RTCSessionDescription;
var attachMediaStream  = exports.attachMediaStream ;
var RTCPeerConnection = exports.RTCPeerConnection;
var log = debugAgent('agentDial:log');








function AgentDial (opts){

    //call WildEmitter Constructor

    var self=this;

    navigator.getUserMedia = navigator.getUserMedia ||navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

    log(navigator, 'Navigator');
    this.peerConnection=null;
    this.isChannelReady = false;
    this.isInitiator = false;
    this.isStarted   = false;
    this.remoteStream='';

    // PeerConnection
    var config = this.config = {
        url: 'http://localhost:8181',

        socketio: {},
        socket: null,
        adjustPeerVolume: true,
        peerVolumeWhenSpeaking: 0.25,
        companyToken:null,
        room:null,
        agentWaitTime:10000,//milliseconds
        media: {
            audio: true
        },
        localAudioClass:'#localAudio',
        remoteAudioClass:'#remoteAudio',
        sdpConstraints:{},
        rtcPeerConfig:{
            'iceServers':[{'url': 'stun:stun.l.google.com:19302'}]
        }
        //please check init again

    };


    this.localAudio=document.querySelector(config.localAudioClass);
    this.remoteAudio=document.querySelector(config.remoteAudioClass);

    //private variables
    var options = opts || {};
    var signal;

    for(var item in options){
        config[item] = options[item];
    }

    log(config.socket, 'config socket');
    //create default socket io connection
    if(config.socket===null){
        signal=this.signal=new SocketIoConnection(config);
        log(signal, 'signal');
    }else {
        signal=this.signal=config.signal;
    }





    //when room is created
    signal.on('created',function(){
        log('Created room ' + self.config.room);
        self.isInitiator = true;
        // Call getUserMedia()
        navigator.getUserMedia(self.config.media,
            self.handleUserMedia.bind(self),
            self.handleUserMediaError.bind(self));
        log('Getting user media with constraints', self.config.rtcPeerConfig);
        self.start(); //basically useless

    });





    signal.on('join',function(){
        log('Another peer made a request to join room ' + self.config.room);
        log('This peer is the initiator of room ' + self.config.room + '!');
        self.isChannelReady = true;

    });

    signal.on('message',function(message){
        log('message:', message);
        if (message === 'got user media') {
            self.start();
        }else if (message.type === 'offer'){
            if(!self.isInitiator && !self.isStarted){
                self.start();
            }
            self.peerConnection.setRemoteDescription(new RTCSessionDescription(message));
            self.answer();

        }else if (message.type === 'candidate' && self.isStarted) {
            var candidate = new RTCIceCandidate({sdpMLineIndex:message.label,
            candidate:message.candidate});
            self.peerConnection.addIceCandidate(candidate);
        } else if (message === 'bye' && self.isStarted) {
            log(typeof self.handleRemoteHangUp, 'hangup');
            self.handleRemoteHangUp();
        }

    });

    signal.on('joined',function(){
        log('This peer has joined room ' + self.config.room);
        self.isChannelReady = true;
        // Call getUserMedia()
        //dont understand why we are calling gt usermedia when stream is already available by this time
        navigator.getUserMedia(self.config.media, self.handleUserMedia.bind(self), self.handleUserMediaError.bind(self));
        log('Getting user media with constraints', self.config.media);

    });

    signal.on('full',function(){
        log('Room is full ....')

    });


    signal.on('log',function(message){
        log.apply(console, message);
    });


    signal.on('hangup',function(message){
        log('Disconnectiong ...')
        self.disconnect();
    })

    signal.on('awaiting agent',function(message){
        log(message, 'No agent yet ...');
    })
}






AgentDial.prototype.initiate=function(roomId,companyToken){

    if(roomId){
        var data = {
            room:"dial",
            companyToken:companyToken,
            agent:true,
            client:false
        };
        log('Create or join room',data.room);

        this.signal.emit('create or join', data);
    }else {
        throw 'Token not received';
    }


};




AgentDial.prototype.start=function(){

    if(!this.isStarted && typeof this.localStream !== 'undefined' && this.isChannelReady) {
        this.createPeerConnection();
        this.isStarted = true;
        if (this.isInitiator) {
            this.call();
        }
    }
};


AgentDial.prototype.handleUserMedia=function(stream){
    this.localStream = stream;
    log(this.localStream);
    log(this.localAudio);
    attachMediaStream(this.localAudio, stream);
    log('Adding local stream.',this, 'skkskkskk');
    this.message('got user media');
};


AgentDial.prototype.createPeerConnection=function(){
    var self=this;
    try {
        this.peerConnection = new RTCPeerConnection(this.config.rtcPeerConfig);
        this.peerConnection.addStream(this.localStream);
        this.peerConnection.onicecandidate = this.handleIceCandidate.bind(self);
        log('Created rtcpeerconnection');

    } catch (error) {
        log('Failed to create PeerConnection, exception: ' + error.message);
        return;
    }
    this.peerConnection.onaddstream = this.handleRemoteStreamAdded.bind(self);
    this.peerConnection.onremovestream = this.handleRemoteStreamRemoved.bind(self);
};




AgentDial.prototype.message=function(message){
    log('Sending message: ', message);
    this.signal.emit('message', message);
};




AgentDial.prototype.handleUserMediaError=function(error){
    log('navigator.getUserMedia error: ', error);
};

AgentDial.prototype.handleIceCandidate=function(event){
    log('handleIceCandidate event: ', event);
    if (event.candidate) {
        this.message({
            type: 'candidate',
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate
        });
    } else {
        log('End of candidates.');
    }

};

AgentDial.prototype.handleRemoteStreamAdded=function(event){
    log('Remote stream added.');
    attachMediaStream(this.remoteAudio, event.stream);
    log('Remote stream attached!!.');
    this.remoteStream = event.stream;

};

AgentDial.prototype.handleRemoteStreamRemoved=function(event){

    log('Remote stream removed',event);

};

AgentDial.prototype.call=function(){
    var self=this;
    log('Creating Offer...');
    this.peerConnection.createOffer(this.setLocalAndSendMessage.bind(self),
        this.onSignalingError.bind(self),
        this.config.sdpConstraints);

};

AgentDial.prototype.handleRemoteHangUp=function(){
    log('Session terminated.');
    this.disconnect();
    this.isInitiator = false;
};

AgentDial.prototype.disconnect=function(){
    this.isStarted = false;
    if (this.peerConnection) {
        this.peerConnection.close();
    }
    this.peerConnection = null;
};

// Signaling error handler
AgentDial.prototype.onSignalingError=function(error) {
    log('Failed to create signaling message : ' + error.name);
};

// Success handler for both createOffer()
// and createAnswer()
AgentDial.prototype.setLocalAndSendMessage=function(sessionDescription) {
    log(sessionDescription, 'sessionDescription');
    this.peerConnection.setLocalDescription(sessionDescription);
    this.message(sessionDescription);
};


AgentDial.prototype.answer=function() {
    log('Sending answer to peer.');
    this.peerConnection.createAnswer(
        this.setLocalAndSendMessage.bind(this),
        this.onSignalingError.bind(this), this.config.sdpConstraints);
}
module.exports=AgentDial;

