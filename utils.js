"use strict";


var utils={
    noop:function(){return;},
    SIGNAL_HOST:'',
    browser:(function() {
        if (window.mozRTCPeerConnection) {
          return 'Firefox';
        } else if (window.webkitRTCPeerConnection) {
          return 'Chrome';
        } else if (window.RTCPeerConnection) {
          return 'Supported';
        } else {
          return 'Unsupported';
        }
      })(),
    randomToken: function () {
        return Math.random().toString(36).substr(2);
  },
  //
  isSecure: function() {
    return location.protocol === 'https:';
  }
}
module.exports=utils