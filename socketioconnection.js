var io=require('./exports').io;





//constructor function

function SocketIoConnection(config){
  this.connection=io.connect(config.url,config.socketio);
}


SocketIoConnection.prototype.on = function (event, callback) {
    this.connection.on(event, callback);
};

SocketIoConnection.prototype.emit = function () {
    this.connection.emit.apply(this.connection, arguments);
};

SocketIoConnection.prototype.getSessionid = function () {
    return this.connection.socket.sessionid;
};

SocketIoConnection.prototype.disconnect = function () {
    return this.connection.disconnect();
};

module.exports = SocketIoConnection;
