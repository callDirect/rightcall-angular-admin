module.exports=function(grunt){
    'use strict';
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        browserify:{
            options:{
                browserifyOptions:{
                    standalone:'<%= pkg.name %>'
                }
            },
            bundle:{
                src:'agentDial.js',
                dest:'agentDial.bundle.js'
            }
        },
        uglify:{
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            minify:{
                src:'agentDial.bundle.js',
                dest:'agentDial.min.js'
            }
        },
        copy:{
            demo:{
                src:'agentDial.min.js',
                dest:'demo/scripts/'
            }
        },
        watch:{
            options:{
                livereload:33729
            },
            dial:{
                files:'agentDial.js',
                tasks:['default'],
            },
            widget:{
                files:'demo/scripts/widget.js',
            },
            html:{
                files:'demo/index.html',
            },
            gruntfile:{
                files:'Gruntfile.js',
                tasks:['default']
            }
        },
        connect:{

            livereload: {
                options:{
                    port:8080,
                    open:true
                },
            }

        }
    });

    grunt.registerTask('default', ['browserify','uglify','copy:demo']);


    grunt.registerTask('build',['default',
        'connect:livereload',
        'watch'
    ]);

};
