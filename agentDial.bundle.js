(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.AgentDial = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
var utils = require('./utils');
var exports = require('./exports');
var SocketIoConnection  = require('./socketioconnection');
var debugCallDirect = exports.debugCallDirect;
var RTCIceCandidate= exports.RTCIceCandidate;
var RTCSessionDescription = exports.RTCSessionDescription;
var attachMediaStream  = exports.attachMediaStream ;
var RTCPeerConnection = exports.RTCPeerConnection;
var log = debugAgent('agentDial:log');








function AgentDial (opts){

    //call WildEmitter Constructor

    var self=this;

    navigator.getUserMedia = navigator.getUserMedia ||navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

    log(navigator, 'Navigator');
    this.peerConnection=null;
    this.isChannelReady = false;
    this.isInitiator = false;
    this.isStarted   = false;
    this.remoteStream='';

    // PeerConnection
    var config = this.config = {
        url: 'http://localhost:8181',

        socketio: {},
        socket: null,
        adjustPeerVolume: true,
        peerVolumeWhenSpeaking: 0.25,
        companyToken:null,
        room:null,
        agentWaitTime:10000,//milliseconds
        media: {
            audio: true
        },
        localAudioClass:'#localAudio',
        remoteAudioClass:'#remoteAudio',
        sdpConstraints:{},
        rtcPeerConfig:{
            'iceServers':[{'url': 'stun:stun.l.google.com:19302'}]
        }
        //please check init again

    };


    this.localAudio=document.querySelector(config.localAudioClass);
    this.remoteAudio=document.querySelector(config.remoteAudioClass);

    //private variables
    var options = opts || {};
    var signal;

    for(var item in options){
        config[item] = options[item];
    }

    log(config.socket, 'config socket');
    //create default socket io connection
    if(config.socket===null){
        signal=this.signal=new SocketIoConnection(config);
        log(signal, 'signal');
    }else {
        signal=this.signal=config.signal;
    }





    //when room is created
    signal.on('created',function(){
        log('Created room ' + self.config.room);
        self.isInitiator = true;
        // Call getUserMedia()
        navigator.getUserMedia(self.config.media,
            self.handleUserMedia.bind(self),
            self.handleUserMediaError.bind(self));
        log('Getting user media with constraints', self.config.rtcPeerConfig);
        self.start(); //basically useless

    });





    signal.on('join',function(){
        log('Another peer made a request to join room ' + self.config.room);
        log('This peer is the initiator of room ' + self.config.room + '!');
        self.isChannelReady = true;

    });

    signal.on('message',function(message){
        log('message:', message);
        if (message === 'got user media') {
            self.start();
        }else if (message.type === 'offer'){
            if(!self.isInitiator && !self.isStarted){
                self.start();
            }
            self.peerConnection.setRemoteDescription(new RTCSessionDescription(message));
            self.answer();

        }else if (message.type === 'candidate' && self.isStarted) {
            var candidate = new RTCIceCandidate({sdpMLineIndex:message.label,
            candidate:message.candidate});
            self.peerConnection.addIceCandidate(candidate);
        } else if (message === 'bye' && self.isStarted) {
            log(typeof self.handleRemoteHangUp, 'hangup');
            self.handleRemoteHangUp();
        }

    });

    signal.on('joined',function(){
        log('This peer has joined room ' + self.config.room);
        self.isChannelReady = true;
        // Call getUserMedia()
        //dont understand why we are calling gt usermedia when stream is already available by this time
        navigator.getUserMedia(self.config.media, self.handleUserMedia.bind(self), self.handleUserMediaError.bind(self));
        log('Getting user media with constraints', self.config.media);

    });

    signal.on('full',function(){
        log('Room is full ....')

    });


    signal.on('log',function(message){
        log.apply(console, message);
    });


    signal.on('hangup',function(message){
        log('Disconnectiong ...')
        self.disconnect();
    })

    signal.on('awaiting agent',function(message){
        log(message, 'No agent yet ...');
    })
}






AgentDial.prototype.initiate=function(roomId,companyToken){

    if(roomId){
        var data = {
            room:"dial",
            companyToken:companyToken,
            agent:true,
            client:false
        };
        log('Create or join room',data.room);

        this.signal.emit('create or join', data);
    }else {
        throw 'Token not received';
    }


};




AgentDial.prototype.start=function(){

    if(!this.isStarted && typeof this.localStream !== 'undefined' && this.isChannelReady) {
        this.createPeerConnection();
        this.isStarted = true;
        if (this.isInitiator) {
            this.call();
        }
    }
};


AgentDial.prototype.handleUserMedia=function(stream){
    this.localStream = stream;
    log(this.localStream);
    log(this.localAudio);
    attachMediaStream(this.localAudio, stream);
    log('Adding local stream.',this, 'skkskkskk');
    this.message('got user media');
};


AgentDial.prototype.createPeerConnection=function(){
    var self=this;
    try {
        this.peerConnection = new RTCPeerConnection(this.config.rtcPeerConfig);
        this.peerConnection.addStream(this.localStream);
        this.peerConnection.onicecandidate = this.handleIceCandidate.bind(self);
        log('Created rtcpeerconnection');

    } catch (error) {
        log('Failed to create PeerConnection, exception: ' + error.message);
        return;
    }
    this.peerConnection.onaddstream = this.handleRemoteStreamAdded.bind(self);
    this.peerConnection.onremovestream = this.handleRemoteStreamRemoved.bind(self);
};




AgentDial.prototype.message=function(message){
    log('Sending message: ', message);
    this.signal.emit('message', message);
};




AgentDial.prototype.handleUserMediaError=function(error){
    log('navigator.getUserMedia error: ', error);
};

AgentDial.prototype.handleIceCandidate=function(event){
    log('handleIceCandidate event: ', event);
    if (event.candidate) {
        this.message({
            type: 'candidate',
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate
        });
    } else {
        log('End of candidates.');
    }

};

AgentDial.prototype.handleRemoteStreamAdded=function(event){
    log('Remote stream added.');
    attachMediaStream(this.remoteAudio, event.stream);
    log('Remote stream attached!!.');
    this.remoteStream = event.stream;

};

AgentDial.prototype.handleRemoteStreamRemoved=function(event){

    log('Remote stream removed',event);

};

AgentDial.prototype.call=function(){
    var self=this;
    log('Creating Offer...');
    this.peerConnection.createOffer(this.setLocalAndSendMessage.bind(self),
        this.onSignalingError.bind(self),
        this.config.sdpConstraints);

};

AgentDial.prototype.handleRemoteHangUp=function(){
    log('Session terminated.');
    this.disconnect();
    this.isInitiator = false;
};

AgentDial.prototype.disconnect=function(){
    this.isStarted = false;
    if (this.peerConnection) {
        this.peerConnection.close();
    }
    this.peerConnection = null;
};

// Signaling error handler
AgentDial.prototype.onSignalingError=function(error) {
    log('Failed to create signaling message : ' + error.name);
};

// Success handler for both createOffer()
// and createAnswer()
AgentDial.prototype.setLocalAndSendMessage=function(sessionDescription) {
    log(sessionDescription, 'sessionDescription');
    this.peerConnection.setLocalDescription(sessionDescription);
    this.message(sessionDescription);
};


AgentDial.prototype.answer=function() {
    log('Sending answer to peer.');
    this.peerConnection.createAnswer(
        this.setLocalAndSendMessage.bind(this),
        this.onSignalingError.bind(this), this.config.sdpConstraints);
}
module.exports=AgentDial;


},{"./exports":2,"./socketioconnection":6,"./utils":7}],2:[function(require,module,exports){
exports.debugCallDirect = window.debugAgent = require('debug');
exports.RTCSessionDescription = window.RTCSessionDescription
exports.RTCIceCandidate =  window.RTCIceCandidate
exports.attachMediaStream  = window.attachMediaStream
exports.RTCPeerConnection =  window.RTCPeerConnection
exports.io  = window.io
},{"debug":3}],3:[function(require,module,exports){

/**
 * This is the web browser implementation of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = require('./debug');
exports.log = log;
exports.formatArgs = formatArgs;
exports.save = save;
exports.load = load;
exports.useColors = useColors;
exports.storage = 'undefined' != typeof chrome
               && 'undefined' != typeof chrome.storage
                  ? chrome.storage.local
                  : localstorage();

/**
 * Colors.
 */

exports.colors = [
  'lightseagreen',
  'forestgreen',
  'goldenrod',
  'dodgerblue',
  'darkorchid',
  'crimson'
];

/**
 * Currently only WebKit-based Web Inspectors, Firefox >= v31,
 * and the Firebug extension (any Firefox version) are known
 * to support "%c" CSS customizations.
 *
 * TODO: add a `localStorage` variable to explicitly enable/disable colors
 */

function useColors() {
  // is webkit? http://stackoverflow.com/a/16459606/376773
  return ('WebkitAppearance' in document.documentElement.style) ||
    // is firebug? http://stackoverflow.com/a/398120/376773
    (window.console && (console.firebug || (console.exception && console.table))) ||
    // is firefox >= v31?
    // https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
    (navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31);
}

/**
 * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
 */

exports.formatters.j = function(v) {
  return JSON.stringify(v);
};


/**
 * Colorize log arguments if enabled.
 *
 * @api public
 */

function formatArgs() {
  var args = arguments;
  var useColors = this.useColors;

  args[0] = (useColors ? '%c' : '')
    + this.namespace
    + (useColors ? ' %c' : ' ')
    + args[0]
    + (useColors ? '%c ' : ' ')
    + '+' + exports.humanize(this.diff);

  if (!useColors) return args;

  var c = 'color: ' + this.color;
  args = [args[0], c, 'color: inherit'].concat(Array.prototype.slice.call(args, 1));

  // the final "%c" is somewhat tricky, because there could be other
  // arguments passed either before or after the %c, so we need to
  // figure out the correct index to insert the CSS into
  var index = 0;
  var lastC = 0;
  args[0].replace(/%[a-z%]/g, function(match) {
    if ('%%' === match) return;
    index++;
    if ('%c' === match) {
      // we only are interested in the *last* %c
      // (the user may have provided their own)
      lastC = index;
    }
  });

  args.splice(lastC, 0, c);
  return args;
}

/**
 * Invokes `console.log()` when available.
 * No-op when `console.log` is not a "function".
 *
 * @api public
 */

function log() {
  // this hackery is required for IE8/9, where
  // the `console.log` function doesn't have 'apply'
  return 'object' === typeof console
    && console.log
    && Function.prototype.apply.call(console.log, console, arguments);
}

/**
 * Save `namespaces`.
 *
 * @param {String} namespaces
 * @api private
 */

function save(namespaces) {
  try {
    if (null == namespaces) {
      exports.storage.removeItem('debug');
    } else {
      exports.storage.debug = namespaces;
    }
  } catch(e) {}
}

/**
 * Load `namespaces`.
 *
 * @return {String} returns the previously persisted debug modes
 * @api private
 */

function load() {
  var r;
  try {
    r = exports.storage.debug;
  } catch(e) {}
  return r;
}

/**
 * Enable namespaces listed in `localStorage.debug` initially.
 */

exports.enable(load());

/**
 * Localstorage attempts to return the localstorage.
 *
 * This is necessary because safari throws
 * when a user disables cookies/localstorage
 * and you attempt to access it.
 *
 * @return {LocalStorage}
 * @api private
 */

function localstorage(){
  try {
    return window.localStorage;
  } catch (e) {}
}

},{"./debug":4}],4:[function(require,module,exports){

/**
 * This is the common logic for both the Node.js and web browser
 * implementations of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = debug;
exports.coerce = coerce;
exports.disable = disable;
exports.enable = enable;
exports.enabled = enabled;
exports.humanize = require('ms');

/**
 * The currently active debug mode names, and names to skip.
 */

exports.names = [];
exports.skips = [];

/**
 * Map of special "%n" handling functions, for the debug "format" argument.
 *
 * Valid key names are a single, lowercased letter, i.e. "n".
 */

exports.formatters = {};

/**
 * Previously assigned color.
 */

var prevColor = 0;

/**
 * Previous log timestamp.
 */

var prevTime;

/**
 * Select a color.
 *
 * @return {Number}
 * @api private
 */

function selectColor() {
  return exports.colors[prevColor++ % exports.colors.length];
}

/**
 * Create a debugger with the given `namespace`.
 *
 * @param {String} namespace
 * @return {Function}
 * @api public
 */

function debug(namespace) {

  // define the `disabled` version
  function disabled() {
  }
  disabled.enabled = false;

  // define the `enabled` version
  function enabled() {

    var self = enabled;

    // set `diff` timestamp
    var curr = +new Date();
    var ms = curr - (prevTime || curr);
    self.diff = ms;
    self.prev = prevTime;
    self.curr = curr;
    prevTime = curr;

    // add the `color` if not set
    if (null == self.useColors) self.useColors = exports.useColors();
    if (null == self.color && self.useColors) self.color = selectColor();

    var args = Array.prototype.slice.call(arguments);

    args[0] = exports.coerce(args[0]);

    if ('string' !== typeof args[0]) {
      // anything else let's inspect with %o
      args = ['%o'].concat(args);
    }

    // apply any `formatters` transformations
    var index = 0;
    args[0] = args[0].replace(/%([a-z%])/g, function(match, format) {
      // if we encounter an escaped % then don't increase the array index
      if (match === '%%') return match;
      index++;
      var formatter = exports.formatters[format];
      if ('function' === typeof formatter) {
        var val = args[index];
        match = formatter.call(self, val);

        // now we need to remove `args[index]` since it's inlined in the `format`
        args.splice(index, 1);
        index--;
      }
      return match;
    });

    if ('function' === typeof exports.formatArgs) {
      args = exports.formatArgs.apply(self, args);
    }
    var logFn = enabled.log || exports.log || console.log.bind(console);
    logFn.apply(self, args);
  }
  enabled.enabled = true;

  var fn = exports.enabled(namespace) ? enabled : disabled;

  fn.namespace = namespace;

  return fn;
}

/**
 * Enables a debug mode by namespaces. This can include modes
 * separated by a colon and wildcards.
 *
 * @param {String} namespaces
 * @api public
 */

function enable(namespaces) {
  exports.save(namespaces);

  var split = (namespaces || '').split(/[\s,]+/);
  var len = split.length;

  for (var i = 0; i < len; i++) {
    if (!split[i]) continue; // ignore empty strings
    namespaces = split[i].replace(/\*/g, '.*?');
    if (namespaces[0] === '-') {
      exports.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
    } else {
      exports.names.push(new RegExp('^' + namespaces + '$'));
    }
  }
}

/**
 * Disable debug output.
 *
 * @api public
 */

function disable() {
  exports.enable('');
}

/**
 * Returns true if the given mode name is enabled, false otherwise.
 *
 * @param {String} name
 * @return {Boolean}
 * @api public
 */

function enabled(name) {
  var i, len;
  for (i = 0, len = exports.skips.length; i < len; i++) {
    if (exports.skips[i].test(name)) {
      return false;
    }
  }
  for (i = 0, len = exports.names.length; i < len; i++) {
    if (exports.names[i].test(name)) {
      return true;
    }
  }
  return false;
}

/**
 * Coerce `val`.
 *
 * @param {Mixed} val
 * @return {Mixed}
 * @api private
 */

function coerce(val) {
  if (val instanceof Error) return val.stack || val.message;
  return val;
}

},{"ms":5}],5:[function(require,module,exports){
/**
 * Helpers.
 */

var s = 1000;
var m = s * 60;
var h = m * 60;
var d = h * 24;
var y = d * 365.25;

/**
 * Parse or format the given `val`.
 *
 * Options:
 *
 *  - `long` verbose formatting [false]
 *
 * @param {String|Number} val
 * @param {Object} options
 * @return {String|Number}
 * @api public
 */

module.exports = function(val, options){
  options = options || {};
  if ('string' == typeof val) return parse(val);
  return options.long
    ? long(val)
    : short(val);
};

/**
 * Parse the given `str` and return milliseconds.
 *
 * @param {String} str
 * @return {Number}
 * @api private
 */

function parse(str) {
  str = '' + str;
  if (str.length > 10000) return;
  var match = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(str);
  if (!match) return;
  var n = parseFloat(match[1]);
  var type = (match[2] || 'ms').toLowerCase();
  switch (type) {
    case 'years':
    case 'year':
    case 'yrs':
    case 'yr':
    case 'y':
      return n * y;
    case 'days':
    case 'day':
    case 'd':
      return n * d;
    case 'hours':
    case 'hour':
    case 'hrs':
    case 'hr':
    case 'h':
      return n * h;
    case 'minutes':
    case 'minute':
    case 'mins':
    case 'min':
    case 'm':
      return n * m;
    case 'seconds':
    case 'second':
    case 'secs':
    case 'sec':
    case 's':
      return n * s;
    case 'milliseconds':
    case 'millisecond':
    case 'msecs':
    case 'msec':
    case 'ms':
      return n;
  }
}

/**
 * Short format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function short(ms) {
  if (ms >= d) return Math.round(ms / d) + 'd';
  if (ms >= h) return Math.round(ms / h) + 'h';
  if (ms >= m) return Math.round(ms / m) + 'm';
  if (ms >= s) return Math.round(ms / s) + 's';
  return ms + 'ms';
}

/**
 * Long format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function long(ms) {
  return plural(ms, d, 'day')
    || plural(ms, h, 'hour')
    || plural(ms, m, 'minute')
    || plural(ms, s, 'second')
    || ms + ' ms';
}

/**
 * Pluralization helper.
 */

function plural(ms, n, name) {
  if (ms < n) return;
  if (ms < n * 1.5) return Math.floor(ms / n) + ' ' + name;
  return Math.ceil(ms / n) + ' ' + name + 's';
}

},{}],6:[function(require,module,exports){
var io=require('./exports').io;





//constructor function

function SocketIoConnection(config){
  this.connection=io.connect(config.url,config.socketio);
}


SocketIoConnection.prototype.on = function (event, callback) {
    this.connection.on(event, callback);
};

SocketIoConnection.prototype.emit = function () {
    this.connection.emit.apply(this.connection, arguments);
};

SocketIoConnection.prototype.getSessionid = function () {
    return this.connection.socket.sessionid;
};

SocketIoConnection.prototype.disconnect = function () {
    return this.connection.disconnect();
};

module.exports = SocketIoConnection;

},{"./exports":2}],7:[function(require,module,exports){
"use strict";


var utils={
    noop:function(){return;},
    SIGNAL_HOST:'',
    browser:(function() {
        if (window.mozRTCPeerConnection) {
          return 'Firefox';
        } else if (window.webkitRTCPeerConnection) {
          return 'Chrome';
        } else if (window.RTCPeerConnection) {
          return 'Supported';
        } else {
          return 'Unsupported';
        }
      })(),
    randomToken: function () {
        return Math.random().toString(36).substr(2);
  },
  //
  isSecure: function() {
    return location.protocol === 'https:';
  }
}
module.exports=utils
},{}]},{},[1])(1)
});